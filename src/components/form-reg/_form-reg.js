/*
	форма Регистрации
	----------------------------------------------
	Форма отправляется через AJAX на адрес обработчик, указанный в атрибуте [action]
	при привязке вёрстки, необходимо закомментировать setTimeout и разкомментировать строчку $.post
	----------------------------------------------
	использованные библиотеки:
	- http://robinherbots.github.io/Inputmask/
	- https://github.com/andr-04/inputmask-multi
	- https://silviomoreto.github.io/bootstrap-select/
	- https://getbootstrap.com/docs/3.3/javascript/#tabs
	- https://getbootstrap.com/docs/3.3/javascript/#modals
	---------------------------------------------- */
(function() {

	var listCountries = $.masksSort($.masksLoad("phone-codes.json"), ['#'], /[0-9]|#/, "mask");

	var maskOpts = {
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			placeholder: '.',
			showMaskOnHover: false,
			autoUnmask: false,
			clearMaskOnLostFocus: false,
			oncomplete: function() {
			},
			onincomplete: function() {
			}
		},
		match: /[0-9]/,
		replace: '#',
		listKey: "mask"
	};



	$('[data-form="reg"]').each(function() {
		var $form = $(this).find('form');
		var $inps = $form.find('[type="hidden"], [type="text"], [type="email"], [type="tel"], [type="password"], [type="checkbox"], select');
		var $btn = $form.find('button[type="submit"]');
		var $modal = $form.parents('.modal');
		var $tab = $form.parents('[data-role="tab"]');

		var $phone_code = $inps.filter('[name="code"]');
		var $phone_number = $inps.filter('[name="phone"]');
		var $currency = $inps.filter('[name="currency"]');


		$form.find('.form-group').each(function() {
			var $group = $(this);
			var $inp = $group.find('.form-control').filter('[type="text"], [type="email"], [type="tel"], [type="password"], textarea');
			var $label = $group.find('.control-label');

			var id = 'inp-' + ($inp.attr('id') || Math.floor(Math.random() * 9999999));

			$label.attr('for', id);
			$inp.attr('id', id);
		});


		var checkCountry = function() {
			var currency = $phone_code.find('option:selected').attr('data-currency');
			var country = $phone_code.val();

			$currency.selectpicker('val', currency);
			$tab.siblings('[data-role="tab"]').trigger('changeCurrencyEvent', currency);
		};


		$phone_code.on('change', function() {
			var code = $(this).val();
			$phone_number
				.val('')
				.filter(':visible').focus();
			$tab.siblings('[data-role="tab"]').trigger('changeCodeEvent', code);
		});

		$currency.on('change', function() {
			var currency = $(this).val();
			$tab.siblings('[data-role="tab"]').trigger('changeCurrencyEvent', currency);
		});


		//оформление селектов
		$inps.filter('select').selectpicker();
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) $inps.filter('select').selectpicker('mobile');


		//маска для ввода телефона
		$phone_number.inputmasks($.extend(true, {}, maskOpts, {
			list: listCountries,
			onMaskChange: function(maskObj, determined) {
				if (determined) {
					var value = maskObj["cc"];
					$phone_code
						.selectpicker('val', value)
						.selectpicker('refresh');
					$tab.siblings('[data-role="tab"]').trigger('changeCountryEvent', value);
				}
				checkCountry();
			}
		}));


		//маска для ввода цифр
		$inps.filter('[data-inputmask]').inputmask({
			showMaskOnHover: false,
			rightAlign: false
		});


		//автофокус в поле ввода Каптчи
		$form.find('[aria-controls="tab-captcha_quick"], [aria-controls="tab-captcha_full"]').on('shown.bs.tab', function() {
			$inps.filter('[name="captcha"]').focus();
		});


		//смена типа у поля Пароль
		$form.find('[data-button="password"]').on('click', function() {
			var type = $inps.filter('[name="password"]').attr('type');
			$(this).removeClass('toggled').addClass(type=='password'?'toggled':'');
			$inps.filter('[name="password"]').attr('type', type=='password'?'text':'password');
		});


		//Синхронизация между вкладками модального окна / Это необязательно конечно, но удобнее
		$phone_number.on('keyup', function() {
			var number = $(this).val();
			$tab.siblings('[data-role="tab"]').trigger('changeNumberEvent', number);
		});

		$tab.bind('changeCountryEvent', function(e, code) {
			$phone_code.selectpicker('val', code);
		});

		$tab.bind('changeCurrencyEvent', function(e, currency) {
			$currency.selectpicker('val', currency);
		});

		$tab.bind('changeCodeEvent', function(e, code) {
			$phone_code.selectpicker('val', code);
			$phone_number.val('');
		});

		$tab.bind('changeNumberEvent', function(e, number) {
			$phone_number.val(number);
		});


		//проверка доступности сабмита
		var check = function() {
			var enable = true;

			$inps.filter('[type="text"], [type="email"], [type="tel"]').filter(':visible').filter('[required]').each(function() {
				var $inp = $(this);
				var value = $inp.val();
				var is_phone = typeof $inp.attr('data-mask')!='undefined';
				if (is_phone) {
					if (!value || value.indexOf('.')>0) enable = false;
				} else {
					if (!value || value=='') enable = false;
				}
			});

			$inps.filter('[type="checkbox"]').filter('[required]').each(function() {
				var $checkbox = $(this);
				var is_checked = $checkbox.is(':checked');
				if (!is_checked) enable = false;
			});

			$inps.filter('select').filter('[required]').each(function() {
				var $select = $(this);
				var value = $select.val();
				if (!value) enable = false;
			});

			$btn.attr('disabled', !enable);
		};


		//отправка формы
		//данные будут отправлена аяксом на php-обработчик, находящийся по адресу, прописанному в атрибуте [action] у тега [form]
		//для демонстрации установлена timeOut
		//для отправки реальных данных надо разкомментировать строку с POST и закомментировать setTimeout
		var send = function() {
			var data = $form.serialize();
			$form.addClass('form--loading');
			console.log(data);
			// $.post(action, data, function(response) {
			setTimeout(function() {
				$form
					.removeClass('form--loading')
					.find('[data-step="data"]').hide().end()
					.find('[data-step="response"]').show();
			}, 500);
			// });
		};


		//сброс формы
		var reset = function() {
			$inps.filter('[type="text"], [type="tel"], [type="password"], [type="email"], textarea').val('');
			$inps.filter('[type="checkbox"]').prop('checked', false);
			$form
				.removeClass('form--loading')
				.find('[data-step="response"]').hide().end()
				.find('[data-step="data"]').show();
			check();
		};

		$inps.filter('select').on('change', function() {
			check();
		});

		$inps.filter('[type="checkbox"]', function() {
			check();
		});

		$inps.filter('[required]').on('input', function() {
			check();
		});

		$tab.find('[aria-controls]').on('shown.bs.tab', function() {
			$inps.filter('[name="captcha"]').attr('disabled', !$inps.filter('[name="captcha"]').is(':visible'));
			check();
		});

		$modal
			.on('shown.bs.modal', function() {
				check();
			})
			.on('hidden.bs.modal', function() {
				reset();
			});

		$form.on('submit', function(event) {
			event.preventDefault();
			send();
		});

		$(document).bind('tabEvent', function() {
			check();
		});

	});


	$('.modal').find('[aria-controls]').on('shown.bs.tab', function() {
		$(document).trigger('tabEvent');
	});

})();
//----------------------------------------------


