/*
	модальные окна - компенсирование сдвига
	использованные библиотеки:
	- https://getbootstrap.com/docs/3.3/javascript/#modals
	------------------------------------------------------------- */
(function() {

	var div = document.createElement('div');
	div["style"]["overflowY"]	= 'scroll';
	div["style"]["width"] = '50px';
	div["style"]["height"] = '50px';
	div["style"]["visibility"] = 'hidden';
	document["body"].appendChild(div);
	var scroll_width = div["offsetWidth"] - div["clientWidth"];
	document["body"].removeChild(div);

	var $body = $('body');
	var $elements = $('[data-role="header"]');

	var set = function() {
		$elements.css('left', -scroll_width);
	};

	var unset = function() {
		$elements.css('left', 0);
	};

	$('.modal')
		.on('show.bs.modal',function() {
			set();
		})
		.on('hidden.bs.modal',function() {
			unset();
		});

})();
// ------------------------------------------------------------


